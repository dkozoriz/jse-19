package ru.t1.dkozoriz.tm.model.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

public final class Project extends BusinessModel implements IWBS {

    public Project() {
        super();
    }

    public Project(final String name) {
        super(name);
    }

    public Project(final String name, final Status status) {
        super(name, status);
    }

}