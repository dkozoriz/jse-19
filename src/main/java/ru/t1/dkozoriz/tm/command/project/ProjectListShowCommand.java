package ru.t1.dkozoriz.tm.command.project;

import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    private static final String NAME = "project-list";

    private static final String DESCRIPTION = "show project list.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(sort);
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}