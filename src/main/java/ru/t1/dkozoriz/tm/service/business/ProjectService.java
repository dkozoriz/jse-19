package ru.t1.dkozoriz.tm.service.business;

import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;

public final class ProjectService extends BusinessService<Project, IProjectRepository> implements IProjectService {

    private final static String NAME = "Project";

    protected String getName() {
        return NAME;
    }

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }


}