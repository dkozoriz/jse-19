package ru.t1.dkozoriz.tm.service.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.business.IBusinessRepository;
import ru.t1.dkozoriz.tm.api.service.business.IBusinessService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.service.AbstractService;

import java.util.Comparator;
import java.util.List;

public abstract class BusinessService<T extends BusinessModel, R extends IBusinessRepository<T>>
        extends AbstractService<T, R> implements IBusinessService<T> {

    public BusinessService(final R abstractRepository) {
        super(abstractRepository);
    }

    public T changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final T model = findById(id);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        return model;
    }

    public T changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        final T model = findByIndex(index);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        return model;
    }

    public T findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        T model = repository.findByIndex(index);
        if (model == null) throw new EntityException(getName());
        return repository.findByIndex(index);
    }

    public List<T> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<? super IWBS> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    public List<T> findAll(final Comparator<? super IWBS> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    public T removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    public T updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final T model = findById(id);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    public T updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final T model = findByIndex(index);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        return model;
    }

}