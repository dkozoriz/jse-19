package ru.t1.dkozoriz.tm.repository.business;

import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.model.business.Project;

import java.util.List;

public final class ProjectRepository extends BusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(final List<Project> models) {
        super(models);
    }

}