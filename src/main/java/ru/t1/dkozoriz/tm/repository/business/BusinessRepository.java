package ru.t1.dkozoriz.tm.repository.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.repository.AbstractRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class BusinessRepository<T extends BusinessModel> extends AbstractRepository<T> implements IAbstractRepository<T> {

    public BusinessRepository(final List<T> models) {
        super(models);
    }

    public List<T> findAll(final Comparator<? super IWBS> comparator) {
        final List<T> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

}