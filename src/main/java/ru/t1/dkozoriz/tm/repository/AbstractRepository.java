package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.List;

public abstract class AbstractRepository<T extends AbstractModel> implements IAbstractRepository<T> {

    protected final List<T> models;

    public AbstractRepository(final List<T> models) {
        this.models = models;
    }

    @Override
    public T add(final T model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<T> findAll() {
        return models;
    }

    public T findById(final String id) {
        for (final T model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    public T findByIndex(final Integer index) {
        return models.get(index);
    }

    public int getSize() {
        return models.size();
    }

    public T remove(final T model) {
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    public T removeById(final String id) {
        final T model = findById(id);
        if (model == null) return null;
        remove(model);
        return model;
    }

    public T removeByIndex(final Integer index) {
        final T model = findByIndex(index);
        if (model == null) return null;
        remove(model);
        return model;
    }

}