package ru.t1.dkozoriz.tm.repository.business;

import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends BusinessRepository<Task> implements ITaskRepository {

    public TaskRepository(final List<Task> models) {
        super(models);
    }

    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

}