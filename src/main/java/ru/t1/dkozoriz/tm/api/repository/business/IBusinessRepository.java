package ru.t1.dkozoriz.tm.api.repository.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<T extends BusinessModel> extends IAbstractRepository<T> {

    T findByIndex(Integer index);

    T removeByIndex(Integer index);

    List<T> findAll(Comparator<? super IWBS> comparator);

}