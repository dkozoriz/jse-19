package ru.t1.dkozoriz.tm.api.repository;

import java.util.List;

public interface IAbstractRepository<T> {

    List<T> findAll();

    T add(T project);

    void clear();

    T findById(String id);

    int getSize();

    T remove(T project);

    T removeById(String id);

}