package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.User;

public interface IUserService extends IAbstractService<User> {

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}