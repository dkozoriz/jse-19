package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}