package ru.t1.dkozoriz.tm.api.service.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.service.IAbstractService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<T extends BusinessModel> extends IAbstractService<T> {

    T removeByIndex(Integer index);

    T create(String name, String description);

    List<T> findAll(Sort sort);

    List<T> findAll(Comparator<? super IWBS> comparator);

    T updateById(String id, String name, String description);

    T updateByIndex(Integer index, String name, String description);

    T findByIndex(Integer index);

    T changeStatusById(String id, Status status);

    T changeStatusByIndex(Integer index, Status status);

}